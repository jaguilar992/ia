import socket
import cv2
import time
host = '192.168.137.20'
port_control = 8001

direcciones = [
  stop = b'000'
  ,avan = b'100'
  ,izq  = b'010'
  ,der  = b'001'
]

def mover_auto(dircc):
    socket_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    socket_server.connect((host, port_control))
    socket_server.sendall(dircc)
    socket_server.close()
