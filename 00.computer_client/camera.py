# -*- coding: utf-8 -*-
import socket
import cv2
import numpy as np

host = '192.168.137.20'
port_camera = 8000

def get_image():
    socket_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    socket_server.connect((host, port_camera))
    conexion = socket_server.makefile('rb')
    stream = b''
    while True:
      stream += conexion.read(1024)
      first = stream.find(b'\xff\xd8')
      last = stream.find(b'\xff\xd9')
      if first != -1 and last != -1:
        break
    conexion.close()
    socket_server.close()
    jpg = stream[first:last+2]
    image = cv2.imdecode(np.frombuffer(jpg, dtype=np.uint8), cv2.IMREAD_COLOR)
    return image
