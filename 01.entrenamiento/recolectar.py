import os
import pygame as pg
import time
import camera
import control
import cv2
import numpy as np

pg.init()
ventana = pg.display.set_mode((270, 100))
pg.display.set_caption('Control')

# Etiquetas
labels = np.zeros((4,4), 'float')
for i in range(4):
    labels[i,i] = 1

# Vectores entrenamiento
X = np.empty((0, 370*240))
y = np.empty((0,4))

salir = False
while not salir:
  image = camera.get_image()
  cv2.imshow('Imagen', image)
  h, w = image.shape
  image_vector = image.reshape(1, h * w)
  events = pg.event.get()

  # Control
  for event in events:
    if event.type == pg.KEYDOWN:
      # DIRECCION IZQUIERDA
      if event.key == pg.K_LEFT:
          control.mover_auto(control.IZQD)
          print('IZQUIERDA')

          # Almacenar vectores, imagen y salida
          X = np.vstack((X, image_vector))
          y = np.vstack((y, labels[2]))

      # DIRECCION DERECHA
      if event.key == pg.K_RIGHT:
          control.mover_auto(control.DERC)
          print('DERECHA')

          # Almacenar vectores, imagen y salida
          X = np.vstack((X, image_vector))
          y = np.vstack((y, labels[3]))

      # DIRECCION ADELANTE
      if event.key == pg.K_UP:
          control.mover_auto(control.AVAN)
          print('AVANZAR')

          # Almacenar vectores, imagen y salida
          X = np.vstack((X, image_vector))
          y = np.vstack((y, labels[1]))

      # DIRECCION STOP
      if event.key == pg.K_DOWN:
          control.mover_auto(control.STOP)
          print('STOP')

          # Almacenar vectores, imagen y salida
          X = np.vstack((X, image_vector))
          y = np.vstack((y, labels[0]))

      if event.key == pg.K_q:
          print('Saliendo...')
          salir = True
  time.sleep(0.1)

pg.quit()

file_name = str(int(time.time()))
directory = "training_data"
if not os.path.exists(directory):
    os.makedirs(directory)
try:
    np.savez(directory + '/' + file_name + '.npz', train=X, train_labels=y)
except IOError as e:
    print(e)
