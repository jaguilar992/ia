import socket
import cv2
import time
host = '192.168.137.20'
port_control = 8001

STOP = b'1000'
AVAN = b'0100'
IZQD = b'0010'
DERC = b'0001'

def mover_auto(dircc):
    socket_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    socket_server.connect((host, port_control))
    socket_server.sendall(dircc)
    socket_server.close()
