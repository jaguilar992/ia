# -*- coding: utf-8 -*-
import socket
import picamera
from camera_hilo import HiloCamera
import time

PORT = 8001
HOST = '0.0.0.0'


class CameraServer:
  def __init__(self):
    self.camera = picamera.PiCamera()
    self.camera.start_preview()
    time.sleep(2)
    self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    self.socket.bind((HOST, PORT))
    self.socket.listen(0)

  def serve(self):
    while True:
      conn, addr = self.socket.accept()
      hilo = HiloCamera(conn, self.camera)
      hilo.run()

c = CameraServer()
c.serve()
