from threading import Thread
import struct
import time
import io

class HiloCamera(Thread):
  def __init__(self, conexion, camera):
    Thread.__init__(self)
    self.conexion = conexion.makefile('wb')
    self.camera = camera
    self.stream = io.BytesIO()

  def run(self):
    try:
      self.camera.capture(self.stream ,'jpeg', resize = (270,100))
      size = self.stream.tell()
      print(size)
      self.stream.seek(0)
      self.conexion.write(self.stream.read())
     cola = 1024 - (size % 1024)
     print (cola)
     for i in range(cola):
       self.conexion.write('\x00')
      self.stream.seek(0)
      self.stream.truncate()
    finally:
      self.conexion.close()
      print("Conexion cerrada")

